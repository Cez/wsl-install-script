@echo off
cd %~dp0
echo setup dir %~dp0
rem  test internet connection
ping 8.8.8.8 -n 2 -w 15 || goto :NoInternet
:forceNoInternet

rem  check if wsl is enabled
IF EXIST "C:\Windows\System32\wsl.exe" (
    echo WSL enabled, proceeding with installation
    goto :proceedAfterReboot
) ELSE (
    echo Reboot will be necessary to continue installation
	rem TODO probably could set up an automated task to run the install once after reboot
    echo Please run the Installation process again after reboot
    Powershell -Command "& { Start-Process \".\src\EnableWSL.bat\" -verb RunAs}"
)
goto :end

:proceedAfterReboot
rem  copy setup files
mkdir C:\Arch\tmp
echo copy setup files
xcopy .\src\* C:\Arch\tmp\ /s /i /h

rem  download arch 
echo checking for previous Arch installation
rem  TODO ask for upgrade
IF EXIST "C:\Arch\Arch.exe" (
    echo Arch already installed
) ELSE (
    if exist C:\Arch\tmp\Arch.zip (
        echo Arch.zip already exists
    ) else (
        echo Downloading ArchWSL
        powershell -Command "bitsadmin /transfer Downloading_ArchWSL /dynamic /download /priority FOREGROUND https://github.com/yuk7/ArchWSL/releases/latest/download/Arch.zip C:\Arch\tmp\Arch.zip"
    )
    powershell -Command "Expand-Archive 'C:\Arch\tmp\Arch.zip' -DestinationPath c:\Arch"
)

rem  set modname1=%userprofile%
rem  echo %modname1% is new user name
rem echo %userprofile% > C:\Arch\tmp\userprofile.txt

IF EXIST "C:\Windows\System32\wsl.exe" (
    echo Run Arch setup
    C:\Arch\Arch.exe
    echo Run user setup
rem      Powershell -Command "& { Start-Process \".\src\userpath.bat\" -wait}"
rem      C:\Arch\Arch.exe run /mnt/c/Arch/tmp/user_setup.sh
) ELSE (
    echo wsl should be enabled but you are missing C:\Windows\System32\wsl.exe, enable it manually and try again
    goto :end
)
cd %userprofile%
rem  echo %~dp0
Powershell -Command "& { Start-Process \"C:\Arch\tmp\userpath.bat\" -WorkingDirectory '%~dp0' -wait}"
C:\Arch\Arch.exe run /mnt/c/Arch/tmp/user_setup.sh

rem  setup username in arch
set /p modname1=< C:\Arch\tmp\username.txt || echo user name from arch: %modname1%
IF EXIST "C:\Windows\System32\wsl.exe" (
	C:\Arch\Arch.exe config --default-user %modname1%
	mkdir C:\Arch\tmp\fonts\UbuntuMono
	powershell -Command "bitsadmin /transfer Downloading_FontScript /dynamic /download /priority FOREGROUND https://github.com/powerline/fonts/raw/master/install.ps1 C:\Arch\tmp\fonts\install.ps1"
	powershell -Command "bitsadmin /transfer Downloading_UbuntuFont /dynamic /download /priority FOREGROUND https://github.com/powerline/fonts/raw/master/UbuntuMono/Ubuntu%%20Mono%%20derivative%%20Powerline.ttf 'C:\Arch\tmp\fonts\UbuntuMono\Ubuntu Mono derivative Powerline.ttf'"
rem  	start C:\Arch\tmp\win-terminal.msixbundle
	echo Set up user %modname1%, need to do some stuff in elevated shell, don't close this window
	Powershell -Command "& { Start-Process \"C:\Arch\tmp\ElavatedShell.bat\" -verb RunAs -Wait}" || echo Can't run elevated shell on this system, start src/ElavatedShell.bat as Administrator
	C:\Arch\Arch.exe run /mnt/c/Arch/tmp/optional.sh
)

rem  setup wsl-tty shortcuts
rem  cd C:\ProgramData\chocolatey\lib\wsltty\tools\wslttyinstall
rem  set proces="C:\ProgramData\chocolatey\lib\wsltty\tools\wslttyinstall\wslinstall.bat C:\Arch\wsltty"
rem  Powershell -Command "& { Start-Process %proces% -WorkingDirectory 'C:\ProgramData\chocolatey\lib\wsltty\tools\wslttyinstall' -wait}"
rem  del "%homepath%\Desktop\WSL Terminal.lnk"
rem  copy "%APPDATA%\Microsoft\Windows\Start Menu\Programs\Arch Terminal.lnk" "%homepath%\Desktop\Terminal.lnk"
rem  del "%APPDATA%\Microsoft\Windows\Start Menu\Programs\WSL Terminal.lnk"
rem  mkdir "%APPDATA%\mintty"
rem  copy C:\Arch\tmp\user_files\.minttyrc "%APPDATA%\mintty\config"
rem  set TARGET="C:\Arch\wsltty\bin\mintty.exe C:\Arch\wsltty\bin\wslbridge2.exe"
rem  set SHORTCUT="C:\Arch\tmp\Terminal.lnk"
rem  set PWS=powershell.exe -ExecutionPolicy Bypass -NoLogo -NonInteractive -NoProfile
rem  %PWS% -Command "$ws = New-Object -ComObject WScript.Shell; $s = $ws.CreateShortcut(%SHORTCUT%); $S.TargetPath = %TARGET%; $S.Save()"
rem  copy %SHORTCUT% "%USERPROFILE%\Desktop\"
cscript C:\Arch\tmp\CreateShortcut.vbs
copy C:\Arch\Terminal.lnk "%homepath%\Desktop\Terminal.lnk"
copy C:\Arch\tmp\settings.json "%USERPROFILE%\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\"
goto :end

:NoInternet
echo No internet connection detected.
set /p fin=Force install? [y/n]:
IF "%fin%" == "y" goto :forceNoInternet

:end
echo All done

PAUSE
