#!/bin/bash
{
	# check if user exists and skip all this
# 	if [ ! -f /mnt/c/Arch/tmp/username.txt ]; then
	pth=$(cat /mnt/c/Arch/tmp/userprofile.txt | tr -d '\r'); echo home path is $pth
	user_name=${pth##*/}
	orig_nm=$user_name
	echo "Starting user setup, your user name is $user_name, continue? Type 'y' to continue (default:exit)"
	read cnt
	if [ $cnt == "y" ]; then
		echo "Create a password for your user (same password will be used for root), I suggest using the same password you use for windows to login to this machine."
		echo "."
		unset pass
		prompt="Enter Password:"
		while IFS= read -p "$prompt" -r -s -n 1 char
		do
			if [[ $char == $'\0' ]]
			then
				break
			fi
			prompt='*'
			pass+="$char"
		done
		echo ""
# 		read -s pass
		# reuse password
		echo "root:$pass" | chpasswd
		# modify sudoers file
		sed -e 's/# %wheel ALL=(ALL) ALL/ %wheel ALL=(ALL) ALL/g' /etc/sudoers > /mnt/c/Arch/tmp/tempsu
		visudo -q -c -f /mnt/c/Arch/tmp/tempsu && cat /mnt/c/Arch/tmp/tempsu > /etc/sudoers # this will fail if the syntax is incorrect
		visudo -c && rm /mnt/c/Arch/tmp/tempsu
# 		user_name=$(echo "$1" | tr '[:upper:]' '[:lower:]')
#TODO make a legal name abbreviation function instead on this
		while [[ $user_name =~ [^a-z0-9_-] ]]; do
			echo "User name '$user_name' contains illegal characters, pick something else (no capital letters, special chr, spaces)"
			echo "Enter new user name:"
			read user_name
		done
		# write username for cmd to configure user later
		echo add user: $user_name
		echo $user_name > /mnt/c/Arch/tmp/username.txt
		useradd -m -G wheel -s /bin/bash $user_name
		# change home directory to windows user dir
		sed -i -e "s/$user_name:x:1000:1000::\/home\/$user_name:\/bin\/bash/$user_name:x:1000:1000::\/mnt\/c\/Users\/$orig_nm:\/bin\/bash/g" /etc/passwd
		cat /etc/passwd >> /mnt/c/Arch/tmp/passwd.log
		# reuse password
		echo "$user_name:$pass" | chpasswd
		# write wsl config so system mounts with metadata to make it possible to chmod windows files
		cat /mnt/c/Arch/tmp/wsl.conf > /etc/wsl.conf
	# 	fi
	# download windows terminal
	# 	if [ ! -f /mnt/c/Arch/tmp/win-terminal.msixbundle ]; then 
	# 		wget https://github.com$(curl -sL  https://github.com/microsoft/terminal/releases | grep msixbundle | head -n1 | cut -d '"' -f 2) -O /mnt/c/Arch/tmp/win-terminal.msixbundle
	# 	fi
		# install wget
		pacman-key --init
		pacman-key --populate
# 		yes | pacman -S wget
		# download fonts
# 		mkdir -p /mnt/c/Arch/tmp/fonts/UbuntuMono
# 		wget https://github.com/powerline/fonts/raw/master/install.ps1 -P /mnt/c/Arch/tmp/fonts
# 		wget https://github.com/powerline/fonts/raw/master/UbuntuMono/Ubuntu%20Mono%20derivative%20Powerline.ttf -P /mnt/c/Arch/tmp/fonts/UbuntuMono
	else 
		echo "ok, bye"
	fi
} | tee /mnt/c/Arch/tmp/install.log
exit
