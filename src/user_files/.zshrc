# TODO hsi

export ZSH=/usr/share/oh-my-zsh
ZSH_THEME="ceznoster"
# Automatically start tmux at the beginning of a session so that you always connect to an existing session
ZSH_TMUX_AUTOSTART="true"
plugins=(history titles tmux)
export zautosuggestions='/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh'

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"
# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"
# Uncomment the following line to disable bi-weekly auto-update checks.
 DISABLE_AUTO_UPDATE="true"
# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=20
# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"
# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"
# Uncomment the following line to enable command auto-correction.
 DISABLE_UNTRACKED_FILES_DIRTY="true"
 ENABLE_CORRECTION="false"
# Uncomment the following line to display red dots whilst waiting for completion.
 COMPLETION_WAITING_DOTS="true"

# Would you like to use another custom folder than $ZSH/custom?
 ZSH_CUSTOM=~/.config/oh-my-zsh-custom
# export MANPATH="/usr/local/man:$MANPATH"
# You may need to manually set your language environment
 export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
 if [[ -n $SSH_CONNECTION ]]; then
   export EDITOR='nano'
 fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

export PATH=~/usr:$PATH
source $ZSH/oh-my-zsh.sh
source $zautosuggestions
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=6'

export HISTFILE=~/.hist_$HOST
export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=20000
export SAVEHIST=20000
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
setopt HIST_BEEP
unsetopt extended_history
unsetopt share_history

####### MJZSH ######
setopt correct                                                  # Auto correct mistakes
setopt extendedglob                                             # Extended globbing. Allows using regular expressions with *
setopt nocaseglob                                               # Case insensitive globbing
setopt rcexpandparam                                            # Array expension with parameters
setopt nocheckjobs                                              # Don't warn about running processes when exiting
setopt numericglobsort                                          # Sort filenames numerically when it makes sense
setopt nobeep                                                   # No beep
setopt appendhistory                                            # Immediately append history instead of overwriting
setopt histignorealldups                                        # If a new command is a duplicate, remove the older one
setopt autocd                                                   # if only directory path is entered, cd there.

zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'       # Case insensitive tab completion
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"         # Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' rehash true                              # automatically find new executables in path
# Speed up completions
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache
export EDITOR=/usr/bin/nano
export VISUAL=/usr/bin/nano
WORDCHARS=${WORDCHARS//\/[&.;]}                                 # Don't consider certain characters part of the word

bindkey '^H' backward-kill-word                                 # delete previous word with ctrl+backspace
bindkey '\C-z' undo                                             # Shift+tab undo last action

alias g='git'
alias s='ssh'
alias c='cmd.exe \c'
alias reload='source ~/.zshrc && echo "reloaded"'

######## SSH #########

export SSH_ADD_DEFAULT_OPTIONS='-t 45000'

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
neofetch

# make gpg agent work through prompt
GPG_TTY=$(tty)
export GPG_TTY
