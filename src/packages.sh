#!/bin/bash
{
	#apt-cyg
	lynx -source rawgit.com/transcode-open/apt-cyg/master/apt-cyg > /tmp/cinst/apt-cyg
	install /tmp/cinst/apt-cyg /bin
	#necessary packages to continue instalation
	apt-cyg install git zsh dos2unix nano
	#oh-my-zsh
	wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -P /tmp/cinst
	#remove "run zsh" from installer
	sed -i '/env zsh/d' /tmp/cinst/install.sh
	sh /tmp/cinst/install.sh
	#load preferences
	dos2unix -n /tmp/cinst/config.ini /tmp/cinst/list
	source /tmp/cinst/list
	ipconfig >> ~/Desktop/info.txt

	#autocomplete and colors
	git clone git://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions
	git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting

	#backup important files, cp .zshrc, scripts, theme, copy with no target directory
	cp ~/.zshrc ~/.zshrc.bak
	cp ~/.gitconfig ~/.gitconfig.bak
	cp ~/.ssh/config ~/.ssh/config.bak
	cp ~/.ssh/authorized_keys ~/.ssh/authorized_keys.bak
	cp /cygdrive/c/Tools/ConEmu/ConEmu.xml /cygdrive/c/Tools/ConEmu/ConEmu.xml.bak
	cp -rT /tmp/cinst/USER ~

	if ! [ -d ~/.ssh ]; then mkdir ~/.ssh; chmod 700 ~/.ssh; touch ~/.ssh/authorized_keys; chmod 600 ~/.ssh/authorized_keys; fi
	#append ssh, git config
	awk '/#sshcfg/{flag=1;next}/#git/{flag=0}flag' /tmp/cinst/appendages >> ~/.ssh/config; chmod 600 ~/.ssh/config
	awk '/#git/{flag=1;next}/#endgit/{flag=0}flag' /tmp/cinst/appendages >> ~/.gitconfig; chmod 600 ~/.gitconfig

	#gitconfig check for user and email

	#install winmerge
	if [ $winmrg = true ]
	then
		winadr=$(lynx -dump -listonly https://bitbucket.org/winmerge/winmerge/downloads | grep -m 1 -E 'x64.*zip' | awk '{ print $2 }' | tr -d '\r')
		wget $winadr -P /tmp/cinst
		winzip=$(basename $winadr)
		apt-cyg install p7zip
		7za x /tmp/cinst/$winzip -o/tmp/cinst
		mkdir /cygdrive/c/Tools
		mv /tmp/cinst/WinMerge /cygdrive/c/Tools
		chmod -R 775 /cygdrive/c/Tools
		echo "Finish installing WinMerge"
	else echo "WinMerge not selected for install"
	fi

	#nano config
	wget https://github.com/scopatz/nanorc/raw/master/zsh.nanorc -P ~/.nano
	wget https://github.com/scopatz/nanorc/raw/master/sh.nanorc -P ~/.nano
	wget https://github.com/scopatz/nanorc/raw/master/python.nanorc -P ~/.nano
	ln -s ~/.nano/zsh.nanorc ~/.nano/zshrc.nanorc

	#speedtest
	wget https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py
	chmod a+rx speedtest.py
	mv speedtest.py usr/speedtest
	chown $USER: usr/speedtest

	#additional packages
	if [ $include_packages = true ]
	then
		echo installing $chosen_packages
		for cpkg in $chosen_packages; do
			apt-cyg install $cpkg
		done
	else echo "additional packages not selected for install"
	fi

	#rtorrent
	if [ $rtrnt = true ]
	then
		apt-cyg install rtorrent
		mkdir ~/rtorrent
		#download config
		wget https://raw.githubusercontent.com/wiki/rakshasa/rtorrent/CONFIG-Template.md -P /tmp/cinst
		#remove comments and set home dir
		cat /tmp/cinst/CONFIG-Template.md | grep -A9999 '^######' | grep -B9999 '^### END' | sed -re "s:/home/USERNAME:$HOME:" > ~/.rtorrent.rc
		#enable udp trackers
		sed -i -e 's/trackers.use_udp.set = no/#trackers.use_udp.set = no/g' ~/.rtorrent.rc
		#less max uploads
		sed -i -e 's/throttle.max_uploads.global.set = 250/throttle.max_uploads.global.set = 100/g' ~/.rtorrent.rc
		#add upload download limits, you might want to change these
		echo "throttle.global_up.max_rate.set_kb = 150" >> ~/.rtorrent.rc
		echo "throttle.global_down.max_rate.set_kb = 2200" >> ~/.rtorrent.rc
	else echo "rtorrent not selected for install"
	fi

	#ssh stuff
	if [ $sshd = true ]
	then
		#Run as privelaged user
		Powershell -Command "& { Start-Process \"C:\cygwin64\tmp\cinst\adm_stuff.bat\" -verb RunAs}"
	else echo "ommiting ssh daemon config"
	fi

	if [ $ident = true ]
	then
		apt-cyg install python
		wget https://raw.githubusercontent.com/ccontavalli/ssh-ident/master/ssh-ident -O ~/usr/ssh
		sed -i -e 's/$HOME\/.ssh-ident/$HOME\/.ssh-ident_$HOSTNAME/g' ~/usr/ssh
		chmod 711 ~/usr/ssh
	else echo "ommiting ssh-ident"
	fi

	mkdir -p ~/.ssh/identities/Rem; chmod -R 700 ~/.ssh/identities
	ssh-keygen -q -N "" -C ruser -f ~/.ssh/identities/Rem/id_rsa
	chmod 600 ~/.ssh/identities/Rem/id_rsa*

	#check if there's no identity already
	if [ $key = true ] && ! [ -e ~/.ssh/identities/$USERNAME/id_rsa ]
	then
		#check if there's no defualt key
		if [ -e ~/.ssh/id_rsa ]
		then
			echo "ssh key found in ~/.ssh dir"
		else
			echo "no key found, generating new one"
			ssh-keygen -o -f ~/.ssh/id_rsa -a 32
			chmod 600 ~/.ssh/id_rsa*
		fi
		#save public key to info
		cat ~/.ssh/id_rsa.pub >> ~/Desktop/info.txt
		unix2dos ~/Desktop/info.txt
		if [ $ident = true ]
		then
			echo "moving to default identity"
			mkdir -p ~/.ssh/identities/$USERNAME
			chmod -R 700 ~/.ssh/identities
			mv ~/.ssh/id_rsa* ~/.ssh/identities/$USERNAME
			chmod 600 ~/.ssh/identities/$USERNAME/id_rsa*
		else echo "."
		fi
	else echo "ommiting ssh config"
	fi

	if [ $sshd = true ]
	then
	    echo "Checking if ssh config is finished"
	    sshdpid=$(cat /tmp/cinst/sshd.pid)
	    while [ $sshdpid -ne 0 ]; do
			echo -n "Finish sshd setup (other window) and press Enter (or Ctrl+C to skip)"
			read kk
			sshdpid=$(cat /tmp/cinst/sshd.pid)
	    done
	    #Edit sshd config, set custom port and disable password
		sed -i -e 's/#Port 22/Port 2232/g' /etc/sshd_config
		#check if user doesn't have a blank password
		if [ $sshpass = true ]
		then
			wget https://live.sysinternals.com/psexec.exe -P /tmp/cinst
			chmod 755 /tmp/cinst/psexec.exe
			/tmp/cinst/psexec.exe -user $USER -p "" pwd.exe /accepteula 2>/tmp/cinst/pass
			pass=$(grep -c 'incorrect' /tmp/cinst/pass)
			nopass=$(grep -c 'Account restrictions' /tmp/cinst/pass)
			if [ $pass -eq 1 ]
			then echo "AuthenticationMethods publickey,password" >> /etc/sshd_config
			elif [ $nopass -eq 1 ]
			then echo "User has no password"
			else echo "Error in detecting user password"
			fi
		else echo "No sshd pass"
		fi
		#set ownership and permissions
		chmod 755 $HOME
		chmod 700 ~/.ssh
		touch ~/.ssh/authorized_keys
		chmod 600 ~/.ssh/authorized_keys
	    echo "SSHD setup finished successfully"
	else echo "ommiting ssh daemon config"
	fi

	# lynx -dump -listonly http://farmanager.com/nightly.php/nightly/nightly/nightly | grep -v pdb | grep -m 1 -E '64.*2018.*7z' | awk '{ print $2 }' | tr -d '\r'
	# lynx -dump -listonly https://ffmpeg.zeranoe.com/builds/win64/static/
	# curl ftp://ftp.imagemagick.org/pub/ImageMagick/binaries/ --list-only
	# msiexec /i tightvnc-2.8.8-gpl-setup-64bit.msi /quiet /norestart SET_PASSWORD=1 VALUE_OF_PASSWORD=$vncpass SET_USECONTROLAUTHENTICATION=0 VALUE_OF_USECONTROLAUTHENTICATION=0 SET_ALLOWLOOPBACK=1 VALUE_OF_ALLOWLOOPBACK=1 SET_QUERYTIMEOUT=1 VALUE_OF_QUERYTIMEOUT=20 SET_LOOPBACKONLY=1 VALUE_OF_LOOPBACKONLY=1 SET_ACCEPTHTTPCONNECTIONS=1 VALUE_OF_ACCEPTHTTPCONNECTIONS=0

	#conemu
	if [ $conemu = true ]
	then
		wget https://conemu.github.io/version.ini -P /tmp/cinst
		#read 2 lines after conemu ver, separate by comma, read 3 col, remove carriage
		adr=$(grep -A 2 'ConEmu_Preview_2' /tmp/cinst/version.ini | awk -F "," '/https/ {print $3}' | tr -d '\r')
		#download
		echo Download ConEmu from $adr; wget $adr -P /tmp/cinst
		#get leaf
		bs=$(basename $adr)
		apt-cyg install p7zip
		7za x /tmp/cinst/$bs -o/tmp/cinst/ce
		mkdir /cygdrive/c/Tools/ConEmu
		mv /tmp/cinst/ce/* /cygdrive/c/Tools/ConEmu
		cp /tmp/cinst/ConEmu_cyginst.xml /cygdrive/c/Tools/ConEmu/ConEmu.xml
		wget https://github.com/supermarin/powerline-fonts/raw/master/Meslo/Meslo%20LG%20M%20DZ%20Regular%20for%20Powerline.otf -P /cygdrive/c/Tools/ConEmu/ConEmu
		chmod -R 775 /cygdrive/c/Tools
		mkshortcut /cygdrive/c/Tools/ConEmu/ConEmu64.exe -n ConEmu -d ConEmu64
		mv ConEmu.lnk ~/Desktop
		cygstart /cygdrive/c/Tools/ConEmu/ConEmu64.exe
		echo "Finished installing ConEmu"
	else echo "conemu not selected for install"
	fi
	#rm -rf /tmp/cinst
} | tee /var/log/packages.log
unix2dos -f /var/log/packages.log
exit