#!/bin/bash
{
	#lock packages.sh
	echo $$ > /tmp/cinst/sshd.pid
	#generate a random password for privelaged ssh user
	pass=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13 ; echo '')
	echo Your cyg_server user password is $pass >> ~/Desktop/info.txt
	#configure sshd
	ssh-host-config -y -w $pass
	#change owner of user home folder for sshd to work
	chown $USER: $HOME
	echo 0 > /tmp/cinst/sshd.pid
} | tee /var/log/csshd.log
unix2dos -f /var/log/csshd.log