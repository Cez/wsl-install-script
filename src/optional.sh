#!/bin/bash
{
	#sudo mount -t drvfs C: /mnt/c -o metadata
	echo "Basic system installation is finished, do you want to install optional packages and continue with some additional steps?"
	echo "This is recommended, type 'y' to continue (default:exit)"
	read cnt
	if [ $cnt == "y" ]; then
# 		echo -n "Enter your user password again:"
# 		read -s pass
		unset pass
		xdg-user-dirs-update
		echo "Enter your user "
		prompt="password again:"
		while IFS= read -p "$prompt" -r -s -n 1 char
		do
			if [[ $char == $'\0' ]]
			then
				break
			fi
			prompt='*'
			pass+="$char"
		done
		echo
		echo $pass | sudo -S pacman
		# upgrade system
# 		sudo pacman-key --init
# 		echo pacman key populate
# 		sudo pacman-key --populate
		echo pacman keyring
		yes | sudo pacman -Sy archlinux-keyring
		echo pacman full upgrade
		yes | sudo pacman -Syyu
		# some basic packages and minimal basic-devel (+go) to run pkgbuild and install yay
		echo install additional packages
		echo $pass | sudo -S pacman
		yes | sudo pacman -S zsh git openssh autoconf automake gcc make wget pkgconf patch go speedtest-cli zsh-autosuggestions zsh-syntax-highlighting neofetch tmux #android-tools android-udev
		git config pull.ff only
		# change shell to zsh
		echo change shell to zsh
		echo $pass | chsh -s /bin/zsh
		#sudo sed -i -e 's/\/bin\/bash/\/bin\/zsh/g' /etc/passwd
		if [ -e ~/.ssh/id_rsa ]
		then
			echo "ssh key found in ~/.ssh dir"
		else
			echo "no key found, generating new one"
			ssh-keygen -o -f ~/.ssh/id_rsa -P $pass -a 32
			chmod 600 ~/.ssh/id_rsa*
			cat ~/.ssh/id_rsa.pub >> ~/Desktop/info.txt
			echo "This is your public ssh key, share it with your admin or whatever, the password is the same as user pass" >> ~/Desktop/info.txt
		fi
		# save public key to info
		# Edit sshd config, set custom port and disable password
		echo $pass | sudo -S pacman
		sudo sed -i -e 's/#Port 22/Port 2232/g' /etc/ssh/sshd_config
		sudo sed -i -e 's/#PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
		# generate host keys
		sudo ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
		sudo ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa
		sudo ssh-keygen -f /etc/ssh/ssh_host_ed25519_key -N '' -t ed25519
		sudo chmod 600 /etc/ssh/ssh_host_*
		# install yay aur helper
		cd /mnt/c/Arch/tmp
		git clone https://aur.archlinux.org/yay.git
		cd yay
		yes | makepkg -si
		# install oh-my-zsh and ssh-ident
		yes | yay -S oh-my-zsh-git ssh-ident-git #jq android-sdk-build-tools-29.0.2
		# TODO configure ssh-ident 
		cp -r /mnt/c/Arch/tmp/user_files/. ~
# 		ln -s /usr/bin/ssh-ident ~/usr/ssh
# 		ln -s /usr/bin/ssh-ident ~/usr/scp
		# this is to prevent from the setsockopt IPV6_TCLASS 32: Operation not permitted error
		echo "AddressFamily inet" >> ~/.ssh/config
		chmod 600 ~/.ssh/config
# 		yes "1" | yay -S --nocleanmenu --nodiffmenu micro-bin
		# add theme and fonts to win terminal settings
		# TODO find json file in case of new version
# 		if [ -f ~/AppData/Local/Packages/Microsoft.WindowsTerminal_8wekyb3d8bbwe/LocalState/profiles.json ]; then
# 			sed -e '/^\s*\/\/.*$/d' -e '/^\s*$/d' ~/AppData/Local/Packages/Microsoft.WindowsTerminal_8wekyb3d8bbwe/LocalState/profiles.json | jq '.profiles[2].colorScheme |= "One Half Dark" | .profiles[2].fontFace |= "Ubuntu Mono derivative Powerline"' > /mnt/c/Arch/tmp/profiles.tmp
# 			defprof=$(cat /mnt/c/Arch/tmp/profiles.tmp | jq '.profiles[2].guid' | sed -e 's/^"//' -e 's/"$//')
# 			jq --arg v "$defprof" '.defaultProfile |= $v' /mnt/c/Arch/tmp/profiles.tmp > ~/AppData/Local/Packages/Microsoft.WindowsTerminal_8wekyb3d8bbwe/LocalState/profiles.json 
# 		fi
# 		sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
		#wget $(curl -sL  https://api.github.com/repos/mintty/wsltty/releases/latest | grep browser_download_url | cut -d '"' -f 4) -P /mnt/c/Arch/tmp
	else 
		echo "ok, bye"
	fi
} | tee /mnt/c/Arch/tmp/optional.log
exit
