# Description
**WSL Install Script** is a collection of scripts for downloading, installation and configuration of Arch linux on Windows 10 and some of my favourite open-source tools for Linux

![screenshot](https://gitlab.com/Cez/wsl-install-script/raw/master/src/screenshot.png)


# Premise
Quick, pretty and easy deployment of a linux subsystem able to operate on Windows files (mostly focused on working with git) without the need for a virtualized environment.

# Features
[Arch WSL linux](https://github.com/yuk7/ArchWSL "Arch WSL github"),
[automount with metadata](https://devblogs.microsoft.com/commandline/chmod-chown-wsl-improvements/ "microsoft devblog"),
[WSLtty](https://github.com/mintty/wsltty "mintty github"),
[Z shell](https://www.zsh.org "Z shell Home"),
[oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh "oh-my-zsh github repo"),
[autosuggestions](https://github.com/zsh-users/zsh-autosuggestions "zsh autosuggestions github"),
[syntax-highlightning](https://github.com/zsh-users/zsh-syntax-highlighting "zsh syntax colors github"),
[speedtest](https://github.com/sivel/speedtest-cli "speedtest github"),
[tmux](https://en.wikipedia.org/wiki/Tmux "terminal multiplexer"),
[ssh-ident](https://www.github.com/ccontavalli/ssh-ident "better ssh-agent") and more!

- Some of my .dotfiles for configuration enhancement
- Set home at windows user home
- Fish-shell style autosuggestions and syntax-highlighting for zsh
- Generate a new key in the new OpenSSH format
- Install powerline fonts for a nice-looking theme
- Set up ssh-agent and make it work with multiple identities
- Configure ssh daemon for incoming connections
- Add a minimal .zshrc with a few handy functions
- Set up a git-optimized oh-my-zsh theme
- Customize nano with syntax coloring and some useful settings
- Some gitconfig aliases

# Installation
Run Install.bat, a restart may be necessary, if so then run the Install.bat again after restart and continue with instructions when prompted

# Supported platforms
Windows 10 64bit - 2004 (19041.508) and higher

# Warning
Some things may not work and if you already have any existing wsl or cygwin configs then they might get overwritten so use at your own risk and read through the code before installing anything.

# Known Issues
- Cmd window sometimes freezes, on the "Installing..." part, press Esc if nothing is happening for too long.
- ssh doesn't seem to work on ipv6 for now so I disable that in the ssh config

# Donations
Tips appreciated:) btc address 1B3cRFeh99yQ2GzYaawqiyJF5P3zvkj12P

# License
**WSL Install Script** is distributed under the _GNU GPLv3_ license in order to keep
it free and accessible by everyone.
